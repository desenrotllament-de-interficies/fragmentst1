package dam.android.fabricio.fragmentst1;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.graphics.Color;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity implements View.OnClickListener{
    private FragmentTransaction fragmentTransaction;
    private Fragment fragment1_1;
    private Fragment fragment2_1;
    private Fragment fragment3_1;
    private Fragment fragment1_2;
    private Fragment fragment2_2;
    private Fragment fragment3_2;
    private Fragment fragment1_3;
    private Fragment fragment2_3;
    private Fragment fragment3_3;

    private Button btFragment1_1;
    private Button btFragment2_1;
    private Button btFragment3_1;
    private Button btFragment1_2;
    private Button btFragment2_2;
    private Button btFragment3_2;
    private Button btFragment1_3;
    private Button btFragment2_3;
    private Button btFragment3_3;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        setUI();

    }

    private void setUI() {
        fragment1_1 = new Fragment1();
        fragment2_1 = new Fragment2();
        fragment3_1 = new Fragment3();
        fragment1_2 = new Fragment1();
        fragment2_2 = new Fragment2();
        fragment3_2 = new Fragment3();
        fragment1_3 = new Fragment1();
        fragment2_3 = new Fragment2();
        fragment3_3 = new Fragment3();

        btFragment1_1 = findViewById(R.id.btFragment1_1);
        btFragment1_2 = findViewById(R.id.btFragment1_2);
        btFragment1_3 = findViewById(R.id.btFragment1_3);
        btFragment2_1 = findViewById(R.id.btFragment2_1);
        btFragment2_2 = findViewById(R.id.btFragment2_2);
        btFragment2_3 = findViewById(R.id.btFragment2_3);
        btFragment3_1 = findViewById(R.id.btFragment3_1);
        btFragment3_2 = findViewById(R.id.btFragment3_2);
        btFragment3_3 = findViewById(R.id.btFragment3_3);


        getSupportFragmentManager().beginTransaction().add(R.id.flPrimero, fragment1_1).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.flSegundo, fragment1_2).commit();
        getSupportFragmentManager().beginTransaction().add(R.id.flTercero, fragment1_3).commit();


        btFragment1_1.setOnClickListener(this);
        btFragment1_2.setOnClickListener(this);
        btFragment1_3.setOnClickListener(this);
        btFragment2_1.setOnClickListener(this);
        btFragment2_2.setOnClickListener(this);
        btFragment2_3.setOnClickListener(this);
        btFragment3_1.setOnClickListener(this);
        btFragment3_2.setOnClickListener(this);
        btFragment3_3.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        fragmentTransaction = getSupportFragmentManager().beginTransaction();
        switch(v.getId()) {

            case R.id.btFragment1_1:
                fragmentTransaction.replace(R.id.flPrimero, fragment1_1);
                break;

            case R.id.btFragment2_1:
                fragmentTransaction.replace(R.id.flPrimero, fragment2_1);
                break;

            case R.id.btFragment3_1:
                fragmentTransaction.replace(R.id.flPrimero, fragment3_1);
                break;

            case R.id.btFragment1_2:
                fragmentTransaction.replace(R.id.flSegundo, fragment1_2);
                break;

            case R.id.btFragment2_2:
                fragmentTransaction.replace(R.id.flSegundo, fragment2_2);
                break;

            case R.id.btFragment3_2:
                fragmentTransaction.replace(R.id.flSegundo, fragment3_2);
                break;

            case R.id.btFragment1_3:
                fragmentTransaction.replace(R.id.flTercero, fragment1_3);
                break;

            case R.id.btFragment2_3:
                fragmentTransaction.replace(R.id.flTercero, fragment2_3);
                break;

            case R.id.btFragment3_3:
                fragmentTransaction.replace(R.id.flTercero, fragment3_3);
                break;
        }
        fragmentTransaction.addToBackStack(null);
        fragmentTransaction.commit();

    }
}